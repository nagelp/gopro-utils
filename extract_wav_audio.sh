#!/bin/bash

# Copyright 2020 Patrick Nagel and contributors
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

echo "== Extract WAV audio from MP4s =="
echo

if [[ "${1}" == "" ]] || [[ "${1}" == "-h" ]] || [[ "${1}" == "--help" ]]; then
  echo "Syntax: $0 <file.mp4>
  
This script takes the given .MP4 file, extracts its audio track and creates
a .WAV file with the same name and timestamp next to it."
  exit 0
fi

if [ ! -f "${1}" ]; then
  echo "${1} is not a file."
  exit 1
fi

if ! ffmpeg -version >/dev/null; then
  echo "ffmpeg not found, make sure that it is installed."
  exit 1
fi

echo "- Extracting audio with ffmpeg..."
ffmpeg -i "${1}" "${1%.mp4}.wav" || { echo "ffmpeg failed."; exit 2; }
echo "- Taking over timestamp..."
touch -r "${1}" "${1%.mp4}.wav"
