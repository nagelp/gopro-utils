#!/bin/bash

# Script will list all files with a GoPro name pattern in the current directory.
# Upon confirming by pressing Enter, it will rename them into a sane format, i.e.
# the chapter number goes to the end.

files=$(find . -maxdepth 1 -regextype sed -regex "\./[gG][a-zA-Z][0-9][0-9][0-9][0-9][0-9][0-9]\..*" | sed -e's|^\./||')

echo "List of files to be renamed:"

for file in ${files}; do
  echo "- ${file}"
done

echo
echo "Press Enter to proceed with the renaming. CTRL-C to abort."
read

for file in ${files}; do
  oldname="${file}"
  newname=$(echo ${oldname} | sed -e's|^\([gG][a-zA-Z]\)\([0-9][0-9]\)\([0-9][0-9][0-9][0-9]\)\.\(.*\)$|\1\3_\2.\4|')
  echo "Renaming ${oldname} --> ${newname}"
  mv ${oldname} ${newname} || { echo "Error occurred. Aborting."; exit 1; }
done
