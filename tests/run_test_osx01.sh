#!/bin/bash

realpath() {
  OURPWD=$PWD
  cd "$(dirname "$1")"
  LINK=$(readlink "$(basename "$1")")
  while [ "$LINK" ]; do
    cd "$(dirname "$LINK")"
    LINK=$(readlink "$(basename "$1")")
  done
  REALPATH="$PWD/$(basename "$1")"
  cd "$OURPWD"
  echo "$REALPATH"
}

scriptdir=$(dirname $(realpath $0))

echo "Adjusting mockfiles/*.MP4 timestamps"
touch -t 201901010000.00 ${scriptdir}/mockfiles/*
touch -t 202001020000.00 ${scriptdir}/mockfiles/GX*0195.MP4
touch -t 202002020000.00 ${scriptdir}/mockfiles/GX*0196.MP4
touch -t 202003020000.00 ${scriptdir}/mockfiles/GX*0197.MP4

echo "Creating temporary directory..."
tempdir=$(mktemp -d)

echo Running ../merge_gopro_mp4s.sh so that only files after March 1st get processed...
echo Should only show file GX*0197.MP4 and successfully process it.

pushd ${scriptdir}/mockfiles >/dev/null
${scriptdir}/../merge_gopro_mp4s.sh ${tempdir} 2020-03-01T00:00:00
echo "------------"
echo "Exit code: $?"
echo
echo "Deleting temporary directory..."
rm -rf ${tempdir}
popd >/dev/null
