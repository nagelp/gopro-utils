#!/bin/bash

scriptdir=$(dirname $(realpath $0))

echo "Adjusting mockfiles/*.MP4 timestamps"
touch -d 2019-01-01T00:00:00 ${scriptdir}/mockfiles/*
touch -d 2020-01-02T00:00:00 ${scriptdir}/mockfiles/GX*0195.MP4
touch -d 2020-02-02T00:00:00 ${scriptdir}/mockfiles/GX*0196.MP4
touch -d 2020-03-02T00:00:00 ${scriptdir}/mockfiles/GX*0197.MP4

echo "Creating temporary directory..."
tempdir=$(mktemp -d)

echo Running ../merge_gopro_mp4s.sh so that only files after March 1st get processed...
echo Should only show file GX*0197.MP4 and successfully process it.

pushd ${scriptdir}/mockfiles >/dev/null
${scriptdir}/../merge_gopro_mp4s.sh ${tempdir} 2020-03-01T00:00:00
echo "------------"
echo "Exit code: $?"
echo
echo "Deleting temporary directory..."
rm -rf ${tempdir}
popd >/dev/null
