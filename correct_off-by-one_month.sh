#!/bin/bash

for i in $@; do
  echo "Changing modification date of ${i}"
  middle_of_given_month=$(date -I -d @$(stat -c %Y $i) | sed -e "s|^\([0-9][0-9][0-9][0-9]-[0-9][0-9]\).*|\1-15|")
  month_minus_one=$(date -I -d "${middle_of_given_month} - 1 month" | sed -s "s|^\([0-9][0-9][0-9][0-9]-[0-9][0-9]\).*|\1|")
  touch -d "$(date -Is -d @$(stat -c %Y $i) | sed -e "s|^\([0-9][0-9][0-9][0-9]-[0-9][0-9]\)|${month_minus_one}|")" $i
done
