#!/bin/bash

# Copyright 2022 Patrick Nagel and contributors
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

echo "== Merge GoPro MP4s (MOV) =="
echo

if [[ "${1}" == "" ]] || [[ "${1}" == "-h" ]] || [[ "${1}" == "--help" ]]; then
  echo "Syntax: $0 <output directory> [<start date/time>]
  
This is a variation of the original merge_gopro_mp4s.sh script. Instead of just
merging the MP4s and keeping them as close as possible to the original files,
this script does the following:

- Take the Stereo audio track the GoPro creates and write out two Mono .WAV format 
  files
- Create a MOV container that contains the (merged) video and the two Mono audio
  tracks and nothing else

Purpose: I'm using the GoPro audio adapter to record two different audio sources
with the GoPro - one on the left and one on the right channel. Thus having them
as separate tracks from the get go makes things easier. Furthermore, I am
using Davinci Resolve Studio on Linux, which cannot import AAC audio tracks, so
having them decompressed to WAV (pcm_s16le) format yet again improves my workflow.

(This replaces the _mkv version of the script, because Davinci Resolve
can't get the timecode (i.e. time of day for easier multi-cam syncing) from MKV
files the way ffmpeg stores it. So moving to MOV, even though I don't like
that container format much)
"
  exit 0
fi

if [ ! -d "${1}" ]; then
  echo "${1} is not an existing directory."
  exit 1
else
  targetdir="${1}"
  echo "Target directory: ${1}"
fi

if ! ffmpeg -version >/dev/null; then
  echo "ffmpeg not found, make sure that it is installed."
  exit 1
fi

# Validate given date
if [[ "${2}" == "" ]]; then
  startdate="1980-01-01T00:00:00"
else
  startdate="${2}"
  if [[ "$OSTYPE" =~ "darwin" ]]; then
    # Even though 'find' on OS X seems to be more flexible than 'date', we need to check that
    # it's a valid date somehow. Currently I see no other way than forcing a specific format
    # onto the user... Would be really nice if GNU date was available on OS X :)
    if ! date -jf %Y-%m-%dT%H:%M:%S "${startdate}" >/dev/null; then
        echo "${startdate} is not a valid date. Please use format \"yyyy-mm-ddThh:mm:ss\"."
        exit 1
    fi
    LC_TIME="en_US.UTF-8"
    echo "Considering only files from $(date -jf %Y-%m-%dT%H:%M:%S "${startdate}") onwards..."
    # Append timezone, or else 'find' will default to UTC.
    startdate="$startdate$(date +%z)"
    echo  
  else
    if ! date -d "${startdate}" >/dev/null; then
        echo "${startdate} is not a valid date."
        exit 1
    fi
    LC_TIME="en_US.UTF-8"
    echo "Considering only files from $(date -d "${startdate}") onwards..."
    echo
  fi
fi

IFS='
'
findpattern='\./G[HXPO][0-9P][0-9R].*\.MP4'
declare -a filelist videonumbers

echo "File list:"
filelist=( $(find . -maxdepth 1 -newermt "${startdate}" -regex "${findpattern}" | sort) )
printf '%s\n' "${filelist[@]}"

echo

echo "Video numbers:"
videonumbers=( $(find . -maxdepth 1 -newermt "${startdate}" -regex "${findpattern}" | sed -e 's|\./G[XHPO][0-9P][0-9R]\(....\)\.MP4$|\1|' | sort -u) )
printf '%s\n' "${videonumbers[@]}"

echo
echo "Press Enter to proceed, Ctrl-C to cancel."
read

echo "Processing videos..."
for videonumber in ${videonumbers[@]}; do
  filelistname="./filelist.tmp"
  echo "WRITE TEST" >"${filelistname}"
  if [ $? != 0 ]; then
    echo "Creating temporary file failed."
    exit 1
  fi
  echo "- Creating temporary ffmpeg file list for video ${videonumber}..."
  find . -maxdepth 1 -newermt "${startdate}" -regex "\./G[HXPO][0-9P][0-9R]${videonumber}\.MP4" | sort | sed -e's|^./|file |' >${filelistname}
  cat $filelistname
  echo "- Running ffmpeg (creating MOV with 2 x pcm_s16le mono audio tracks)..."
  ffmpeg -f concat -i ${filelistname} -filter_complex "[0:a]channelsplit=channel_layout=stereo" -map v:0 -c:v copy -c:a pcm_s16le "${targetdir}/GoPro${videonumber}.mov" || {
    echo "ffmpeg failed (see above)."
    rm -f ${filelistname}
    exit 1;
  }
  echo "- Taking over timestamp..."
  touch -r "$(head -n 1 ${filelistname} | cut -d' ' -f 2)" "${targetdir}/GoPro${videonumber}.mov"
  rm -f ${filelistname}
done

echo
echo "Resulting (merged) file(s):"
for videonumber in ${videonumbers[@]}; do
  ls -lh "${targetdir}/GoPro${videonumber}.mov"
done
